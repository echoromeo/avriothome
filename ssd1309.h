/**
 * \file
 *
 *
 (c) 2018 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms,you may use this software and
    any derivatives exclusively with Microchip products.It is your responsibility
    to comply with third party license terms applicable to your use of third party
    software (including open source software) that may accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIPs TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 */
#ifndef ssd1309_H_INCLUDED
#define ssd1309_H_INCLUDED

#include <atmel_start.h>
#include <i2c_simple_master.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup ssd1309_oled_controller_group ssd1309 OLED Controller Low-level\
 * driver
 *
 * This is a low level driver for the ssd1309 OLED controller through I2C.
 * It provides basic functions for initializing and writing to the OLED
 * controller. In addition to hardware control and use of the OLED controller
 * internal functions.
 *
 * Before writing data to the display call \ref ssd1309_init() which will set up
 * the OLED. 
 * \warning This driver is not reentrant and can not be used in interrupt\
 * service routines without extra care.
 *
 */

//! \name Fundamental Command defines
//@{
#define ssd1309_CMD_SET_LOW_COL(column) (0x00 | (column))
#define ssd1309_CMD_SET_HIGH_COL(column) (0x10 | (column))
#define ssd1309_CMD_SET_MEMORY_ADDRESSING_MODE 0x20
#define ssd1309_CMD_SET_COLUMN_ADDRESS 0x21
#define ssd1309_CMD_SET_PAGE_ADDRESS 0x22
#define ssd1309_CMD_SET_START_LINE(line) (0x40 | (line))
#define ssd1309_CMD_SET_CONTRAST_CONTROL_FOR_BANK0 0x81
#define ssd1309_CMD_SET_CHARGE_PUMP_SETTING 0x8D
#define ssd1309_CMD_SET_SEGMENT_RE_MAP_COL0_SEG0 0xA0
#define ssd1309_CMD_SET_SEGMENT_RE_MAP_COL127_SEG0 0xA1
#define ssd1309_CMD_ENTIRE_DISPLAY_AND_GDDRAM_ON 0xA4
#define ssd1309_CMD_ENTIRE_DISPLAY_ON 0xA5
#define ssd1309_CMD_SET_NORMAL_DISPLAY 0xA6
#define ssd1309_CMD_SET_INVERSE_DISPLAY 0xA7
#define ssd1309_CMD_SET_MULTIPLEX_RATIO 0xA8
#define ssd1309_CMD_SET_DISPLAY_ON 0xAF
#define ssd1309_CMD_SET_DISPLAY_OFF 0xAE
#define ssd1309_CMD_SET_PAGE_START_ADDRESS(page) (0xB0 | (page & 0x07))
#define ssd1309_CMD_SET_COM_OUTPUT_SCAN_UP 0xC0
#define ssd1309_CMD_SET_COM_OUTPUT_SCAN_DOWN 0xC8
#define ssd1309_CMD_SET_DISPLAY_OFFSET 0xD3
#define ssd1309_CMD_SET_DISPLAY_CLOCK_DIVIDE_RATIO 0xD5
#define ssd1309_CMD_SET_PRE_CHARGE_PERIOD 0xD9
#define ssd1309_CMD_SET_COM_PINS 0xDA
#define ssd1309_CMD_SET_VCOMH_DESELECT_LEVEL 0xDB
#define ssd1309_CMD_NOP 0xE3
//@}
//! \name Graphic Acceleration Command defines
//@{
#define ssd1309_CMD_SCROLL_H_RIGHT 0x26
#define ssd1309_CMD_SCROLL_H_LEFT 0x27
#define ssd1309_CMD_CONTINUOUS_SCROLL_V_AND_H_RIGHT 0x29
#define ssd1309_CMD_CONTINUOUS_SCROLL_V_AND_H_LEFT 0x2A
#define ssd1309_CMD_DEACTIVATE_SCROLL 0x2E
#define ssd1309_CMD_ACTIVATE_SCROLL 0x2F
#define ssd1309_CMD_SET_VERTICAL_SCROLL_AREA 0xA3
//@}

#define ssd1309_reset_clear() OLED_RESET_set_level(false);
#define ssd1309_reset_set() OLED_RESET_set_level(true);

// SSD1309 i2c
#define SSD1309_I2C_ADDRESS1 0x3C //SA0 (DC-pin) = 0
#define SSD1309_I2C_ADDRESS2 0x3D //SA0 (DC-pin) = 1

#define SSD1309_I2C_DATA	(1 << 6)
#define SSD1309_I2C_CMD		(0 << 6)

// Resolution
#define SSD1309_PIXELS_W	128
#define SSD1309_PIXELS_H	64

//@{
//#if defined(__DOXYGEN__)

//! \name OLED controller write and read functions
//@{
/**
 * \brief Writes a command to the display controller
 *
 * \param address the i2c address to write to
 * \param command the command to write
 */
static void ssd1309_write_command(uint8_t address, uint8_t command)
{
	I2C_0_write1ByteRegister(address, SSD1309_I2C_CMD, command);
}

/**
 * \brief Write data to the display controller
 *
 * \param address the i2c address to write to
 * \param data the data to write
 */
static inline void ssd1309_write_data(uint8_t address, uint8_t data)
{
	I2C_0_write1ByteRegister(address, SSD1309_I2C_DATA, data);
}

static inline void ssd1309_write_data_buffer(uint8_t address, uint8_t *data, uint8_t len)
{
	I2C_0_writeNByteRegister(address, SSD1309_I2C_DATA, data, len);
}


/**
 * \brief Read data from the controller
 *
 * \note Does the controller support read in I2C mode?
 *
 * \param address the i2c address to write to
 * \retval 8 bit data read from the controller
 */
static inline uint8_t ssd1309_read_data(uint8_t address)
{
	return 0;
}

/**
 * \brief Read status from the controller
 *
 * \note Does the controller support read in I2C mode?
 *
 * \param address the i2c address to write to
 * \retval 8 bit status read from the controller
 */
static inline uint8_t ssd1309_get_status(uint8_t address)
{
	return 0;
}
//@}

//! \name OLED Controller reset
//@{

/**
 * \brief Perform a hard reset of the OLED controller
 *
 * This functions will reset the OLED controller by setting the reset pin low.
 * \note this functions should not be confused with the \ref ssd1309_soft_reset()
 * function, this command will control the RST pin.
 */
static inline void ssd1309_hard_reset(void)
{
	ssd1309_reset_clear();
	for (volatile uint16_t i = 1; i; i++)
		;
	ssd1309_reset_set(); 
	for (volatile uint16_t i = 1; i; i++)
		;
}
//@}

//! \name Sleep control
//@{
/**
 * \brief Enable the OLED sleep mode
 *
 * \param address the i2c address to write to
 */
static inline void ssd1309_set_sleep(uint8_t address, uint8_t enable)
{
	ssd1309_write_command(address, ssd1309_CMD_SET_DISPLAY_OFF | (enable & 0x01));
}

//! \name Address setup for the OLED
//@{
/**
 * \brief Set current page in display RAM
 *
 * This command is usually followed by the configuration of the column address
 * because this scheme will provide access to all locations in the display
 * RAM.
 *
 * \param address the i2c address to write to
 * \param page_address the page address
 */
static inline void ssd1309_set_page_address(uint8_t address, uint8_t page_address)
{
	// Make sure that the address is 4 bits (only 8 pages)
	page_address &= 0x0F;
	ssd1309_write_command(address, ssd1309_CMD_SET_PAGE_START_ADDRESS(page_address));
}

/**
 * \brief Set current column in display RAM
 *
 *
 * \param address the i2c address to write to
 * \param column_address the column address
 */
static inline void ssd1309_set_column_address(uint8_t address, uint8_t column_address)
{
	// Make sure the address is 7 bits
	column_address &= 0x7F;
	ssd1309_write_command(address, ssd1309_CMD_SET_HIGH_COL(column_address >> 4));
	ssd1309_write_command(address, ssd1309_CMD_SET_LOW_COL(column_address & 0x0F));
}

/**
 * \brief Set the display start draw line address
 *
 * This function will set which line should be the start draw line for the OLED.
 *
 * \param address the i2c address to write to
 * \param line_address the start draw line
 */
static inline void ssd1309_set_display_start_line_address(uint8_t address, uint8_t line_address)
{
	// Make sure address is 6 bits
	line_address &= 0x3F;
	ssd1309_write_command(address, ssd1309_CMD_SET_START_LINE(line_address));
}

/**
 * \brief Set the OLED contrast level
 *
 * \param address the i2c address to write to
 * \param contrast a number between 0 and 0xFF
 *
 * \retval contrast the contrast value written to the OLED controller
 */
static inline uint8_t ssd1309_set_contrast(uint8_t address, uint8_t contrast)
{
	ssd1309_write_command(address, ssd1309_CMD_SET_CONTRAST_CONTROL_FOR_BANK0);
	ssd1309_write_command(address, contrast);
	return contrast;
}

/**
 * \brief Invert all pixels on the device
 *
 * This function will invert all pixels on the OLED
 *
 * \param address the i2c address to write to
 */
static inline void ssd1309_display_invert_enable(uint8_t address)
{
	ssd1309_write_command(address, ssd1309_CMD_SET_INVERSE_DISPLAY);
}

/**
 * \brief Disable invert of all pixels on the device
 *
 * This function will disable invert on all pixels on the OLED
 *
 * \param address the i2c address to write to
 */
static inline void ssd1309_display_invert_disable(uint8_t address)
{
	ssd1309_write_command(address, ssd1309_CMD_SET_NORMAL_DISPLAY);
}

/**
 * \brief Clear the screen
 *
 * This function will write 0x00 to all columns in all pages of the screen
 *
 * \param address the i2c address to write to
 */
static inline void ssd1309_clear(uint8_t address)
{
	uint8_t page = 0;
	uint8_t col  = 0;

	for (page = 0; page < (SSD1309_PIXELS_H/8); ++page) {
		ssd1309_set_page_address(address, page);
		ssd1309_set_column_address(address, 0);
		for (col = 0; col < SSD1309_PIXELS_W; ++col) {
			ssd1309_write_data(address, 0x00);
		}
	}
}
//@}

//! \name Initialization
//@{
void ssd1309_init(uint8_t address);
//@}

void oled_clear_whole_buffer( void );
void oled_write_pixel_to_buffer(int8_t x, int8_t y);
void oled_clear_pixel_in_buffer(int8_t x, int8_t y);
void oled_write_buffer_to_screen( uint8_t address );
void oled_clear_screen_and_buffer(uint8_t address);

//! \name Write text routine
//@{
void oled_write_text8(uint8_t address, const char *string);
uint8_t oled_char50_to_buffer(const char c, const int16_t x, const int16_t y);
void oled_text50_to_buffer(const char *string, const uint8_t x, const uint8_t y);
//@}

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ssd1309_H_INCLUDED */
