/*
 * Scan with 8 buttons
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#include "buttons_8.h"

buttons_t buttons[NUM_BUTTONS] = {0};
uint8_t xtoggle = PIN6_bm;

// Call twice to get a full scan
void updateButtons(void)
{
	if (PORTD.OUT & PIN6_bm) // Every other time
	{
		//Find changes since last time
		GPIOR2 = GPIOR0 ^ GPIOR1;

		for (uint8_t i = 0; i < NUM_BUTTONS; i++)
		{
			if (GPIOR2 & (1 << i))	//State change
			{
				if (GPIOR0 & (1 << i))	// New push
				{
					buttons[i].timer = 0;
					buttons[i].flags = 0;
					buttons[i].new_event = true;
				}
				else { // Release
					if (buttons[i].timer < BUTTON_HOLD_TIME)
					{
						buttons[i].click = true;
					} else
					{
						buttons[i].released = true;
					}
				}
			}
			else if (GPIOR0 & (1 << i))	//Still high
			{
				if (buttons[i].timer++ >= BUTTON_HOLD_TIME)
				{
					buttons[i].hold = true;
					//Prevent overflow?
				}
			}
		}
		
		GPIOR1 = GPIOR0;
		GPIOR0 = 0;
		} else { // Every other time
		GPIOR0 <<= PIN4_bp;
	}
	
	// Switch X-line high/low
	PORTD.OUTSET = PIN4_bm | PIN6_bm;
	xtoggle ^= PIN4_bm | PIN6_bm;
	PORTD.OUTCLR = xtoggle;
}

ISR(PORTA_PORT_vect) {
	PORTA.INTFLAGS = PIN0_bm;

	if (VPORTA.IN & PIN0_bm)
	{
		GPIOR0 |= PIN1_bm;
	}
}

ISR(PORTC_PORT_vect) {
	PORTC.INTFLAGS = PIN3_bm | PIN1_bm | PIN0_bm;

	if (VPORTC.IN & PIN0_bm)
	{
		GPIOR0 |= PIN3_bm;
	}
	if (VPORTC.IN & PIN1_bm)
	{
		GPIOR0 |= PIN0_bm;
	}
	if (VPORTC.IN & PIN3_bm)
	{
		GPIOR0 |= PIN2_bm;
	}
}
