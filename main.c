/*
\file   main.c

\brief  Main source file.

(c) 2018 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any
derivatives exclusively with Microchip products. It is your responsibility to comply with third party
license terms applicable to your use of third party software (including open source software) that
may accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
SOFTWARE.
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "main.h"
#include "application_manager.h"
#include "credentials_storage/credentials_storage.h"
#include "led.h"
#include "sensors_handling.h"
#include "cloud/cloud_service.h"
#include "cloud/crypto_client/crypto_client.h"
#include "cloud/mqtt_packetPopulation/mqtt_packetPopulate.h"
#include "debug_print.h"
#include "ssd1309.h"
#include "buttons_8.h"

#define OLED1	SSD1309_I2C_ADDRESS1
#define OLED2	SSD1309_I2C_ADDRESS2

char mqttPublishTopic[MQTT_TOPIC_LENGTH];
const char mqttPayloadOff[] = "off";
const char mqttPayloadClick[] = "click";
const char mqttPayloadHold[] = "hold";
char tempstr[6];

//Subscribe topics will be prepended with "avriot/<mqttCID>/" unless they start with "avriot"
char mqttSubscribeTopics[NUM_TOPICS_SUBSCRIBE][MQTT_TOPIC_LENGTH] = {
	"oled1",
	"oled2",
	"avriot/config/oled_display",
};

static void oled_draw_degrees(uint8_t x, uint8_t y)
{
	for (uint8_t i = x; i < x+12; i++)
	{
		for (uint8_t j = y; j < y+12; j++)
		{
			oled_write_pixel_to_buffer(i,j);
		}
	}

	oled_clear_pixel_in_buffer(x+4, y+4);
	oled_clear_pixel_in_buffer(x+7, y+7);
	oled_clear_pixel_in_buffer(x+4, y+7);
	oled_clear_pixel_in_buffer(x+7, y+4);

	oled_clear_pixel_in_buffer(x+1, y+1);
	oled_clear_pixel_in_buffer(x+10, y+10);
	oled_clear_pixel_in_buffer(x+1, y+10);
	oled_clear_pixel_in_buffer(x+10, y+1);


	for (uint8_t i = 0; i < 4; i++)
	{
		oled_clear_pixel_in_buffer(x+i, y);
		oled_clear_pixel_in_buffer(x+i, y+11);

		oled_clear_pixel_in_buffer(x+11-i, y);
		oled_clear_pixel_in_buffer(x+11-i, y+11);

		oled_clear_pixel_in_buffer(x, y+i);
		oled_clear_pixel_in_buffer(x, y+11-i);

		oled_clear_pixel_in_buffer(x+11, y+i);
		oled_clear_pixel_in_buffer(x+11, y+11-i);

	}

	for (uint8_t i = 0; i < 3; i++)
	{
		oled_clear_pixel_in_buffer(x+6+i, y+5);
		oled_clear_pixel_in_buffer(x+6+i, y+6);
		
		oled_clear_pixel_in_buffer(x+5, y+6+i);
		oled_clear_pixel_in_buffer(x+6, y+6+i);
		
		oled_clear_pixel_in_buffer(x+5-i, y+5);
		oled_clear_pixel_in_buffer(x+5-i, y+6);
		
		oled_clear_pixel_in_buffer(x+5, y+5-i);
		oled_clear_pixel_in_buffer(x+6, y+5-i);
	}
}

void oled_write_temp_from_number(uint8_t address, int16_t temp_e1)
{
	oled_clear_whole_buffer();
	if (temp_e1 < 0)
	{
		oled_char50_to_buffer('-', 0, 50);
	}

	//	int8_t temp5 = temp_e1 % 10;
	temp_e1 = abs(temp_e1);
	
	if (temp_e1 >= 100)
	{
		oled_char50_to_buffer(temp_e1/100 + '0', 20, 50);
	}

	oled_char50_to_buffer((temp_e1/10)%10 + '0', 50, 50);
	oled_char50_to_buffer('.', 76, 51);
	oled_char50_to_buffer(temp_e1%10 + '0', 88, 50);
	oled_draw_degrees(116, 5);

	oled_write_buffer_to_screen(address);
}

void oled_write_temp_from_string(uint8_t address, char *string)
{
	uint8_t length = strlen((const char*)string);

	oled_clear_whole_buffer();
	if (string[0] == '-')
	{
		oled_char50_to_buffer('-', 0, 50);
		string++;
		length--;
	}

	if (string[0] < '0' || string[0] > '9')
	{
		return; // Not supported, unknown?
	}
	
	if (length == 2 || length == 4) // "23" or "23.5"
	{
		oled_char50_to_buffer(string[0], 20, 50);
		string++;
	}

	oled_char50_to_buffer(string[0], 50, 50);
	oled_char50_to_buffer('.', 76, 51);
	if (length > 2)
	{
		oled_char50_to_buffer(string[2], 88, 50);
	} else {
		oled_char50_to_buffer('0', 88, 50);	
	}
	oled_draw_degrees(116, 5);

	oled_write_buffer_to_screen(address);
}

// This handles messages published from the MQTT server when subscribed
void receivedFromCloud(uint8_t *topic, uint8_t *payload)
{
	if (0 == memcmp(topic, mqttSubscribeTopics[0], strlen(mqttSubscribeTopics[0])))
	{
		if (eeprom->oledConfig1 == 1)
		{
			if (payload[0] != 'u') { // HA unavailable
				oled_write_temp_from_string(OLED1, (char *)payload);			
			} else {
				oled_clear_whole_buffer();
				oled_write_buffer_to_screen(OLED1);
			}
		}
		else if (eeprom->oledConfig1) {
			oled_clear_whole_buffer(); 
			oled_text50_to_buffer((char *)payload, 0, 50);
			oled_write_buffer_to_screen(OLED1);		
		}
		
	} else if (0 == memcmp(topic, mqttSubscribeTopics[1], strlen(mqttSubscribeTopics[1]))) {
		if (eeprom->oledConfig2 == 1)
		{
			if (payload[0] != 'u') { // HA unavailable
				oled_write_temp_from_string(OLED2, (char *)payload);			
			} else {
				oled_clear_whole_buffer();
				oled_write_buffer_to_screen(OLED2);
			}
		} 
		else if (eeprom->oledConfig2) {
			oled_clear_whole_buffer();
			oled_text50_to_buffer((char *)payload, 0, 50);
			oled_write_buffer_to_screen(OLED2);
		}
	} else if (0 == memcmp(topic, mqttSubscribeTopics[2], strlen(mqttSubscribeTopics[2]))) {
		uint8_t contrast = 0xff & atoi((const char*)payload);
		if (eeprom->oledConfig1) {
			if (contrast) {
				ssd1309_set_contrast(OLED1, contrast);
				ssd1309_set_sleep(OLED1, 1); //Screen on
			} else {
				ssd1309_set_sleep(OLED1, 0); //Screen off
			}
		}
		if (eeprom->oledConfig2) {
			if (contrast) {
				ssd1309_set_contrast(OLED2, contrast);
				ssd1309_set_sleep(OLED2, 1); //Screen on
			} else {
				ssd1309_set_sleep(OLED2, 0); //Screen off
			}
		}
	}

	debug_printer(SEVERITY_NONE, LEVEL_NORMAL, "topic: %s", topic);
	debug_printer(SEVERITY_NONE, LEVEL_NORMAL, "payload: %s", payload);
}


// This will get called every ~100ms only while we have a valid Cloud connection
void sendToCloud(char *id)
{
	if (eeprom->keysConfig) {
			
		// Update button states only when connected?
		updateButtons();

		// Use ATECC608 ID or predefined CID
		if (eeprom->mqttCID[0] != 0)
		{
			id = eeprom->mqttCID;
		}
	
		// Publish stuff
		for (uint8_t i = 0; i < NUM_BUTTONS; i++)
		{
			if (buttons[i].new_event && (buttons[i].click || buttons[i].hold))
			{
				sprintf(mqttPublishTopic, "avriot/%s/button%d", id, i);
				if (buttons[i].click)
				{
					CLOUD_publishData(mqttPublishTopic, (char *)mqttPayloadClick, sizeof(mqttPayloadClick)-1);
					buttons[i].released = true;
				}
				if (buttons[i].hold)
				{
					CLOUD_publishData(mqttPublishTopic, (char *)mqttPayloadHold, sizeof(mqttPayloadHold)-1);
				}
				LED_flashYellow();
				buttons[i].new_event = false;
				return;
			} 
		}
		
		// Split loop in two because click and hold has priority over release
		for (uint8_t i = 0; i < NUM_BUTTONS; i++)
		{
			if (buttons[i].released && (buttons[i].click || buttons[i].hold))
			{
				sprintf(mqttPublishTopic, "avriot/%s/button%d", id, i);
				CLOUD_publishData(mqttPublishTopic, (char *)mqttPayloadOff, sizeof(mqttPayloadOff)-1);
				LED_flashYellow();
				buttons[i].released = false;
				buttons[i].click = false;
				buttons[i].hold = false;
				return;
			}
		}
	}
}
 
int main(void)
{
	// All the AVR-IoT Init stuff
	application_init();

	// OLED init stuff
	if (eeprom->oledConfig1)
	{
		ssd1309_hard_reset(); //Will reset both
		ssd1309_init(OLED1);
		ssd1309_set_display_start_line_address(OLED1, 0);

		//TODO: Initial display = connection info?
		oled_clear_whole_buffer();
		oled_char50_to_buffer('1', 50, 50);
		oled_write_buffer_to_screen(OLED1);
	}

	if (eeprom->oledConfig2)
	{
		ssd1309_init(OLED2);
		ssd1309_set_display_start_line_address(OLED2, 0);

		//TODO: Initial display = connection info?
		oled_clear_whole_buffer();
		oled_char50_to_buffer('2', 50, 50);
		oled_write_buffer_to_screen(OLED2);
	}

	// Initialize all buttons to send off at first connection
	if (eeprom->keysConfig)
	{
		for (uint8_t i = 0; i < 8; i++)
		{
			buttons[i].hold = true;
			buttons[i].released = true;
		}
	}
	
	while (1) {
		runScheduler();
	}

	return 0;
}

