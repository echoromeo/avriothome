/**
 * \file
 *
 *
 (c) 2018 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms,you may use this software and
    any derivatives exclusively with Microchip products.It is your responsibility
    to comply with third party license terms applicable to your use of third party
    software (including open source software) that may accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIPs TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 */
#include <atmel_start.h>
#include "ssd1309.h"
#include "font.h"
#include <avr/pgmspace.h>
#include <string.h>

/**
 * \brief Initialize the OLED controller
 *
 * Call this function to initialize the hardware interface and the OLED
 * controller. When initialization is done the display is turned on and ready
 * to receive data.
 */
void ssd1309_init(uint8_t address)
{
	// Don't do a hard reset of the OLED display controller here, 
	// since we might probably connect two displays to one reset line
	
	
	// Set MUX ratio to N+1 MUX, (Default => 63)
// 	ssd1309_write_command(address, ssd1309_CMD_SET_MULTIPLEX_RATIO);
// 	ssd1309_write_command(address, SSD1309_PIXELS_H-1);

// 	// Shift Mapping RAM Counter (0x00~0x3F, Default => 0x00)
// 	ssd1309_write_command(address, ssd1309_CMD_SET_DISPLAY_OFFSET);
// 	ssd1309_write_command(address, 0x00);

//	// Set Mapping RAM Display Start Line (0x00~0x3F, Default => 0x00)
//	ssd1309_write_command(address, ssd1309_CMD_SET_START_LINE(0x00));

	// Set Column Address 127 Mapped to SEG0 (Default => 0 mapped to SEG0)
	ssd1309_write_command(address, ssd1309_CMD_SET_SEGMENT_RE_MAP_COL127_SEG0);

	// Set COM/Row Scan Scan from COM[N-1] to COM0, (Default => COM0 to COM[N �1])
	ssd1309_write_command(address, ssd1309_CMD_SET_COM_OUTPUT_SCAN_DOWN);

	// Set COM Pins hardware configuration
	// Bit 4: Alternative COM pin configuration (Default => Alternative)
	// Bit 5: Disable COM Left/Right remap (Default => Disabled)
	// Bit 1: Always 1
// 	ssd1309_write_command(address, ssd1309_CMD_SET_COM_PINS);
// 	ssd1309_write_command(address, (0 << 5) | (1 << 4) | (1 << 1));

	// Disable inverted display (Default => Disabled)
//	ssd1309_display_invert_disable(address);

// 	// Set Display Clock Divide Ratio / Oscillator Frequency (Default => 0x80)
// 	ssd1309_write_command(address, ssd1309_CMD_SET_DISPLAY_CLOCK_DIVIDE_RATIO);
// 	ssd1309_write_command(address, 0x80);

	// Enable charge pump regulator (Does not exist for SSD1309?)
// 	ssd1309_write_command(address, ssd1309_CMD_SET_CHARGE_PUMP_SETTING);
// 	ssd1309_write_command(address, 0x14);

	// Set VCOMH Deselect Level (0x40 Does not exist for SSD1309?)
// 	ssd1309_write_command(address, ssd1309_CMD_SET_VCOMH_DESELECT_LEVEL);
// 	ssd1309_write_command(address, 0x40); // Default => 0x20 (0.77*VCC)

	// Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	// Default is 2 and 2 (caused flickering on my setup)
 	ssd1309_write_command(address, ssd1309_CMD_SET_PRE_CHARGE_PERIOD);
 	ssd1309_write_command(address, (15 << 4) | (1 << 0));

	// Set contrast to minimum (Default => 0x7F)
	ssd1309_set_contrast(address, 0x01);
	
	// Entire display On
	ssd1309_write_command(address, ssd1309_CMD_ENTIRE_DISPLAY_AND_GDDRAM_ON);
	ssd1309_set_sleep(address, 1);
}

 /* Write a character to the screen with font 8 to current position */
static void oled_putchar8(uint8_t address, char character){ // Use ASCII code minus 32, Font8
	ssd1309_write_data_buffer(address, (uint8_t *)font8[character - 32], 8);
}

/**
 * \brief Display text on OLED screen.
 * \param string String to display.
 */
void oled_write_text8(uint8_t address, const char *string)
{
	while (*string != '\0') {
		oled_putchar8(address, *string);
		string++;
	}
}

static uint8_t screen_buffer[SSD1309_PIXELS_H/8][SSD1309_PIXELS_W] = {0};

void oled_clear_whole_buffer( void ) {
	memset(screen_buffer, 0, sizeof(screen_buffer));
}

void oled_write_pixel_to_buffer(int8_t x, int8_t y) {
	if (x < SSD1309_PIXELS_W && y < SSD1309_PIXELS_H)
	{
		uint8_t y_line = y / 8;
		uint8_t y_bit = y % 8;
	
		screen_buffer[y_line][x] |= (1 << y_bit);
	}
}

void oled_clear_pixel_in_buffer(int8_t x, int8_t y) {
	uint8_t y_line = y / (SSD1309_PIXELS_H/8);
	uint8_t y_bit = y % (SSD1309_PIXELS_H/8);
	
	screen_buffer[y_line][x] &= ~(1 << y_bit);
}

void oled_write_buffer_to_screen( uint8_t address ) {
	for (uint8_t i = 0; i < (SSD1309_PIXELS_H/8); i++)
	{
		ssd1309_set_page_address(address, i);
		ssd1309_set_column_address(address, 0);
		ssd1309_write_data_buffer(address, screen_buffer[i], SSD1309_PIXELS_W);
	}		
}

void oled_clear_screen_and_buffer(uint8_t address)
{
	oled_clear_whole_buffer();
	for (uint8_t i = 0; i < (SSD1309_PIXELS_H/8); i++)
	{
		ssd1309_set_page_address(address, i);
		ssd1309_set_column_address(address, 0);
		ssd1309_write_data_buffer(address, screen_buffer[i], SSD1309_PIXELS_W);
	}
}


uint8_t oled_char50_to_buffer(const char c, const int16_t x, const int16_t y) {
    GFXglyph *glyph  = &Roboto_Black_50.glyph[c-Roboto_Black_50.first];
    uint8_t  *bitmap = Roboto_Black_50.bitmap;

    uint16_t bo = glyph->bitmapOffset;
    int8_t  w  = glyph->width, h = glyph->height;
    int8_t   xo = glyph->xOffset, yo = glyph->yOffset;
    int8_t  xx, yy, bits = 0, bit = 0;

    for(yy=0; yy<h; yy++) {
        for(xx=0; xx<w; xx++) {
            if(!(bit++ & 7)) {
                bits = bitmap[bo++];
            }
            if(bits & 0x80) {
				oled_write_pixel_to_buffer(x+xo+xx, y+yo+yy);
            }
            bits <<= 1;
        }
    }
	
	return glyph->xAdvance;
}

/**
 * \brief Display text on OLED screen.
 * \param string String to display.
 */
void oled_text50_to_buffer(const char *string, const uint8_t x, const uint8_t y)
{
	uint8_t i = 0;
	while (*string != '\0') {
		i += oled_char50_to_buffer(*string, x+i, y);
		string++;
	}
}