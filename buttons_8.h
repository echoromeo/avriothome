/*
 * Scan with 8 buttons
 */ 


#ifndef BUTTONS_8_H_
#define BUTTONS_8_H_

#define BUTTON_HOLD_TIME	4	// ~x*200ms = 0.8 sec
#define NUM_BUTTONS			8

typedef struct buttons_td
{
	uint8_t timer;
	union {
		uint8_t flags;
		struct {
			uint8_t new_event	:1;
			uint8_t click		:1;
			uint8_t hold		:1;
			uint8_t released	:1;
			uint8_t reserved	:4;
		};
	};
} buttons_t;

extern buttons_t buttons[NUM_BUTTONS] ;

void updateButtons(void);

#endif /* BUTTONS_8_H_ */