# AVR-IoT Home OLED/Keypad
A project based on the Microchip AVR-IoT board with the ATmega4808 MCU and WINC1510 WiFi module.
It has been modified to connect to any MQTT broker, I have used CloudMQTT and a local Home Assistant server as testing grounds.

|![Temperature display front](docs/display_v1_front.jpg)|![Temperature display back](docs/display_v1_back.jpg)|
|:-:|:-:|
|![Temperature display front](docs/keypad_v2_front.jpg)|![Temperature display back](docs/keypad_v2_back.jpg)|

The two versions I have made are separate displays and keypad, but there should be no problem combining the two.

## Programming the AVR-IoT with this code
The supercool embedded debugger (nEDBG) on the AVR-IoT board with an emulated usb-drive makes it really easy to program the ATmega4808:
- Move/Copy the hex file for the project to the "USB-drive"
- or just program and debug in Atmel Studio without needing an external programmer

## Command-line Interface
The superawesome embedded debugger (nEDBG) on the AVR-IoT board with the combination of a serial COM port and an emulated usb-drive makes it really easy to configure the board. The configurations below can be done in any order:

#### Wifi
1. Put `CMD:SEND_UART=wifi <my_ssid>,<my_password>,my_access_type` in a text file named 'WIFI.CFG'
2. Move/Copy this file to the "USB-drive"

Access type is `2` for PSK and `3` for WEP, and if you are connecting to an open network you just write `CMD:SEND_UART=wifi <my_ssid>`.

#### MQTT
1. Put `CMD:SEND_UART=mqtt <my_username>,<my_password>,<my_client_id>` in a text file named 'MQTT.CFG'
2. Move/Copy this file to the "USB-drive"

If you want to use de unique ID from the ATECC608 as client ID (cid), just write `CMD:SEND_UART=mqtt <my_username>,<my_password>`

#### Broker/Host
1. Put `CMD:SEND_UART=host <my_address>,my_port,my_address_type` in a text file named 'HOST.CFG'
2. Move/Copy this file to the "USB-drive"

If the address is an IP address, use `0` in address type, otherwise use `1` for DNS lookup.

#### Enable features
**PLEASE** read OLED the section below for more details before enabling the OLEDs.

1. Put `CMD:SEND_UART=features my_keyconfig,my_oled1config,my_oled2config` in a text file named 'FEATURES.CFG'
2. Move/Copy this file to the "USB-drive"

Write `0` to disable a feature, and `1` to enable it.
Enabling OLED1 alone is fine, enabling OLED2 alone is not supported.

### Advanced Mode / Troubleshooting
I have had some trouble with the CLI not receiving all the characters when I use this procedure, so if you are having trouble connecting I suggest trying some of the suggestions below.

1. Try to disconnect and reconnect power between each attempt at sending one of the commands.
2. Connect to the serial port using your favourite terminal using 9600 baud and try to get in contact with the CLI and reconfigure the above using the same commands, just without the `CMD:SEND_UART=` at the beginning.
3. Debug using Atmel Studio and/or configure all but the wifi credentials to be put in eeprom directly.

## Keypad
Each button has a separate mqtt topic in the form `avriot/<my_client_id>/buttonX`, where X is a number between 0 and 8.

The code reports key statuses `off`, `click` and `hold`: 
* At first connect after power-up it will report all buttons as `off`
* When a short click is registered it will report a `click` followed shortly `off`
* When a click is longer than ~1 second, a `hold` is reported and the `off` will not be sent until the button is released

The board support up to 9 buttons in a 3x3 key matrix. At the moment only a 4x2 matrix is implemented because that was what I wanted when I wrote the code. I will switch to 3x3 when I make the next prototype.
- Ouput pins: PD4 and PD6
- Input pins: PC0, PC1, PC3 and PA0

#### Pin connection table - Key matrix

|       | SW0 | SW1 | SW2 | SW3 | SW4 | SW5 | SW6 | SW7 | SW8 |
|-------|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|**PD4**|  x  |  x  |  x  |  x  |     |     |     |     |     |
|**PD6**|     |     |     |     |  x  |  x  |  x  |  x  |     |
|**PC1**|  d  |     |     |     |  d  |     |     |     |     |
|**PA0**|     |  d  |     |     |     |  d  |     |     |     |
|**PC0**|     |     |  d  |     |     |     |  d  |     |     |
|**PC3**|     |     |     |  d  |     |     |     |  d  |     |

- `x` means direct connection to one pin on the switch
- `d` means the other pin on the switch in series with a diode, with the cathode connected to the switch pin

I used Cherry MX switches and some old 1N4148 diodes for the first prototype, with the wiring looking like this:
![Temperature display front](docs/keypad_v2_wiring.jpg)

## OLEDs
The code supports one or two 128x64 pixel SSD1309 OLEDS in I2C mode. I have used the 2.42" version.

<div class="panel panel-danger">
**Danger**
{: .panel-heading}
<div class="panel-body">

The OLEDs need to be connected properly before you enable them. If not the I2C code will hang during initalization and block anything else from happening, and since the configuration is stored in eeprom you will not be able to disable them by powercycling or even reprogramming the hex. Programming the elf would work, but do not think the nEDBG supports that. Of course, in Atmel Studio you can reprogram the device and everything will work fine.

</div>
</div>

- To update a display temperature, publish a number string between `-99.9` and `99.9` to `avriot/<my_client_id>/oled1` or `avriot/<my_client_id>/oled2`.
- To turn off the displays, publish the `0` character to `avriot/config/oled_displays`.
- To turn on the displays and set the contrast, publish a number string between `1` and `255` to `avriot/config/oled_displays`.
- At the moment only a temperature display is supported. You can write other stuff than degrees by setting the feature config to `2`. However, only the ascii characters between `' '` and `'9'` are supported at the moment. The flash is almost full.

#### Pin connection table - OLEDs

|       | OLED1 | OLED2 | 
|-------|:-----:|:-----:|  
|**GND**|  GND  |  GND  |  
|**3V3**|  VCC  |  VCC  |  
|**PD7**|  RES  |  RES  | 
|**PA2**|  SDA  |  SDA  | 
|**PA3**|  SCL  |  SCL  |
|  DC  |  GND  |  VCC  |
|  CS  |  GND  |  GND  | 

- DC and CS should be connected locally on the OLED PCB. On my screens, R7 is used to short CS to GND.

<style>
.alert-danger {
  color: rgb(169,68,66) !important;
}
</style>