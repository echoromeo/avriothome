/*
 * main.h
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#include <avr/io.h>

#define MQTT_TOPIC_LENGTH EEPROM_PAGE_SIZE // Because why not consistency
#define NUM_TOPICS_SUBSCRIBE 3 // Defines number of topics which can be subscribed

extern char mqttSubscribeTopics[NUM_TOPICS_SUBSCRIBE][MQTT_TOPIC_LENGTH];

void receivedFromCloud(uint8_t *topic, uint8_t *payload);

// This could be better done with a function pointer (DI) but in the interest of simplicity
//     we avoided that. This is being called from MAIN_dataTask
void sendToCloud(char *id);

#endif /* MAIN_H_ */